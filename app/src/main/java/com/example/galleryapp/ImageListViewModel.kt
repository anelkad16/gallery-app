package com.example.galleryapp

import androidx.lifecycle.ViewModel
import com.example.galleryapp.model.CommonState
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class ImageListViewModel @Inject constructor(
    private val service: ApiService
) : ViewModel() {

    private var _state = MutableStateFlow<CommonState>(CommonState.HideLoading)
    val state: StateFlow<CommonState> = _state

    init {
        getImages()
    }

    fun getImages(page: Int = 1) =
        service.getImageList(page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { list -> _state.value = CommonState.Result(list) },
                { _state.value = CommonState.Error(it.stackTraceToString()) }
            )
}