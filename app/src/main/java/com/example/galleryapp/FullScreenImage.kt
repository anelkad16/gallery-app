package com.example.galleryapp

import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import com.example.galleryapp.databinding.DialogFragmentImageBinding
import com.squareup.picasso.Picasso

class FullScreenImage : DialogFragment(R.layout.dialog_fragment_image) {

    private lateinit var binding: DialogFragmentImageBinding

    companion object {
        @JvmStatic
        fun newInstance(url: String) =
            FullScreenImage().apply {
                arguments = Bundle().apply {
                    putString("url", url)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DialogFragmentImageBinding.bind(view)
        Picasso.get()
            .load(arguments?.getString("url"))
            .placeholder(R.drawable.progress_animation)
            .into(binding.imageView)
    }
}