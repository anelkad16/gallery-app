package com.example.galleryapp

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.galleryapp.databinding.FragmentImageListBinding
import com.example.galleryapp.model.CommonState
import com.example.galleryapp.model.Image
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class ImageListFragment : Fragment(R.layout.fragment_image_list) {

    private lateinit var binding: FragmentImageListBinding
    @Inject
    lateinit var viewModel: ImageListViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as MyApp).appComponent.inject(this)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentImageListBinding.bind(view)
        binding.apply {
            list.apply {
                itemAnimator = null
                adapter = ImageListAdapter(::openFullScreenImage)
            }
            swipeRefresh.setOnRefreshListener {
                viewModel.getImages((1..10).random())
            }
        }

        observeStates()
    }

    private fun observeStates() {
        viewModel.state.onEach { state ->
            when (state) {
                is CommonState.Error -> {
//                    showErrorState(state.error)
                }

                is CommonState.HideLoading -> {
//                    binding.progressBar.isVisible = false
                }

                is CommonState.ShowLoading -> {
//                    showLoadingState()
                }

                is CommonState.Result<*> -> {
                    binding.swipeRefresh.isRefreshing = false
                    (binding.list.adapter as ImageListAdapter).submitList(state.result as List<Image>)
                }
            }
        }.launchIn(lifecycleScope)
    }

    private fun openFullScreenImage(url: String) {
        val fragmentManager = parentFragmentManager
        FullScreenImage.newInstance(url).show(fragmentManager, "dialog")
    }
}