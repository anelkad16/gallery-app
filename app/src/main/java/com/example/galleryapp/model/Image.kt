package com.example.galleryapp.model

import com.google.gson.annotations.SerializedName

data class Image(
    val id: String,
    @SerializedName("download_url") val url: String
)
