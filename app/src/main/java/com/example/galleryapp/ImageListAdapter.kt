package com.example.galleryapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.galleryapp.databinding.ItemImageBinding
import com.example.galleryapp.model.Image

class ImageListAdapter(
    private val onClick: (String) -> Unit
) : ListAdapter<Image, ImageViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {

        return ImageViewHolder(
            ItemImageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            onClick
        )
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }
}

class ImageViewHolder(
    private val binding: ItemImageBinding,
    private val onClick: (String) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Image) {
        binding.imageView.setOnClickListener { onClick(item.url) }
        Glide
            .with(binding.root.context)
            .load(item.url)
            .placeholder(R.drawable.progress_animation)
            .into(binding.imageView)
    }
}

class DiffCallback : DiffUtil.ItemCallback<Image>() {
    override fun areItemsTheSame(oldItem: Image, newItem: Image) = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: Image, newItem: Image) = oldItem == newItem
}
