package com.example.galleryapp

import com.example.galleryapp.model.Image
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("list")
    fun getImageList(
        @Query("page") page: Int = 1
    ): Single<List<Image>>

}
